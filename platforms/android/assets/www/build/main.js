webpackJsonp([0],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlantsListModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_plant__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlantsListModalComponent = /** @class */ (function () {
    function PlantsListModalComponent(app, navParams, viewCtrl, toastCtrl) {
        this.app = app;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.plants = [];
        this.operation = "";
        this.type = "";
        this.list = [];
        this.symbiosis = true;
        this.plants = this.navParams.get("plants");
        this.symbiosis = this.navParams.get('symbiosis');
        console.log('this.symbiosis: ', this.symbiosis);
        this.list = this.navParams.get('list');
        this.type = __WEBPACK_IMPORTED_MODULE_2__services_plant__["b" /* SymbioticType */][Number(this.navParams.get("type"))];
    }
    PlantsListModalComponent.prototype.onAddPlant = function (plant) {
        (this.list.find(function (p) { return p.name == plant.name; })) ? this.presentToast() : this.list.push(plant);
    };
    PlantsListModalComponent.prototype.onRemovePlant = function (plant) {
        this.list = this.list.filter(function (p) { return p.name != plant.name; });
    };
    PlantsListModalComponent.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            position: "top",
            message: 'This plant is already in the list!',
            duration: 3000
        });
        toast.present();
    };
    PlantsListModalComponent.prototype.onDismiss = function () {
        this.viewCtrl.dismiss(this.list);
    };
    PlantsListModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'plants-list-modal',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/components/plants-list-modal/plants-list-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            {{ type }} relationship\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <plants-list [plants]="plants" [symbiosis]="symbiosis" [type]="type" [existedList]="list" (onAddPlantPressed)="onAddPlant($event)" (onRemovePlantPressed)="onRemovePlant($event)"></plants-list>\n\n    <button margin-left ion-button color="secondary" outline (click)="onDismiss()">done</button>\n\n</ion-content>'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/components/plants-list-modal/plants-list-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PlantsListModalComponent);
    return PlantsListModalComponent;
}());

//# sourceMappingURL=plants-list-modal.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 153:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 153;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__suggestion_suggestion__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__suggestion_suggestion__["a" /* SuggestionPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Plants" tabIcon="leaf"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Suggestions" tabIcon="flower"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuggestionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_local_storage__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_plant__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SuggestionPage = /** @class */ (function () {
    function SuggestionPage(navCtrl, loadingCtrl, storageService, plantService) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storageService = storageService;
        this.plantService = plantService;
        this.plants = [];
        this.name = "";
        this.check = {
            noSymbiosis: false,
            hasMutualistic: false,
            hasParasitic: false,
            hasCompetition: false,
            hasCommensalistic: false
        };
    }
    SuggestionPage.prototype.getPlants = function (type) {
        return this.activePlant.symbiosis[__WEBPACK_IMPORTED_MODULE_3__services_plant__["b" /* SymbioticType */][type].toLowerCase()].slice(0, 5);
    };
    SuggestionPage.prototype.search = function () {
        var _this = this;
        this.clear();
        this.plants = this.plantService.getPlants();
        this.plants.forEach(function (p) {
            if (p.name.toLowerCase() === _this.name.toLowerCase()) {
                _this.activePlant = p;
                var hasSymbiosis = false;
                if (p.symbiosis.mutualistic.length > 0) {
                    _this.check.hasMutualistic = true;
                    hasSymbiosis = true;
                }
                if (p.symbiosis.commensalistic.length > 0) {
                    _this.check.hasCommensalistic = true;
                    hasSymbiosis = true;
                }
                if (p.symbiosis.parasitic.length > 0) {
                    _this.check.hasParasitic = true;
                    hasSymbiosis = true;
                }
                if (p.symbiosis.competition.length > 0) {
                    _this.check.hasCompetition = true;
                    hasSymbiosis = true;
                }
                if (!hasSymbiosis) {
                    _this.check.noSymbiosis = true;
                }
            }
        });
    };
    SuggestionPage.prototype.clear = function () {
        this.check.hasMutualistic = false;
        this.check.hasCommensalistic = false;
        this.check.hasParasitic = false;
        this.check.hasCompetition = false;
        this.check.noSymbiosis = false;
    };
    SuggestionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-suggestion',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/suggestion/suggestion.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Suggestions\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-item>\n    <ion-label stacked>Plant name:</ion-label>\n    <ion-input type="text"  (ionChange)="search()" [(ngModel)]="name"></ion-input>\n  </ion-item>\n\n  <p padding class="text" *ngIf="name.length == 0">\n    Enter the plant name to get the best suggestions!\n  </p>\n\n  <p padding class="text" *ngIf="check.noSymbiosis">\n    There is no symbiotic relationship for this plant.\n  </p>\n\n\n  <ion-card margin-bottom *ngIf="check.hasMutualistic">\n    <div class="flex-row">\n      <ion-label padding-left><strong>Mutualistic</strong></ion-label>\n      <button ion-button clear>View All</button>\n    </div>\n    <hr/>\n    <div class="flex-row space-padding">\n      <div class="centered" *ngFor="let plant of getPlants(0)">\n        <img class="img-round" src="{{ plant.image }}">\n        <span>{{ plant.name }}</span>\n      </div>\n    </div>\n  </ion-card>\n\n  <ion-card margin-bottom *ngIf="check.hasParasitic">\n    <div class="flex-row">\n      <ion-label padding-left><strong>Parasitic</strong></ion-label>\n      <button ion-button clear>View All</button>\n    </div>\n    <hr/>\n    <div class="flex-row space-padding">\n      <div class="centered" *ngFor="let plant of getPlants(1)">\n        <img class="img-round" src="{{ plant.image }}">\n        <span>Roses</span>\n      </div>\n    </div>\n  </ion-card>\n\n  <ion-card margin-bottom *ngIf="check.hasCompetition">\n    <div class="flex-row">\n      <ion-label padding-left><strong>Competition</strong></ion-label>\n      <button ion-button clear>View All</button>\n    </div>\n    <hr/>\n    <div class="flex-row space-padding">\n      <div class="centered" *ngFor="let plant of getPlants(2)">\n        <img class="img-round" src="{{ plant.image }}">\n        <span>Roses</span>\n      </div>\n    </div>\n  </ion-card>\n \n  <ion-card margin-bottom *ngIf="check.hasCommensalistic">\n    <div class="flex-row">\n      <ion-label padding-left><strong>Commensalistic</strong></ion-label>\n      <button ion-button clear>View All</button>\n    </div>\n    <hr/>\n    <div class="flex-row space-padding">\n      <div class="centered" *ngFor="let plant of getPlants(3)">\n        <img class="img-round" src="{{ plant.image }}">\n        <span>Roses</span>\n      </div>\n    </div>\n  </ion-card>\n\n\n</ion-content>'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/suggestion/suggestion.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__services_local_storage__["a" /* LocalStorageService */], __WEBPACK_IMPORTED_MODULE_3__services_plant__["a" /* PlantService */]])
    ], SuggestionPage);
    return SuggestionPage;
}());

//# sourceMappingURL=suggestion.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_plant__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__plant_plant_details_plant_details__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__plant_add_plant_add_plant__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_local_storage__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, app, plantService, loadingCtrl, storageService) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.plantService = plantService;
        this.loadingCtrl = loadingCtrl;
        this.storageService = storageService;
        this.initialize();
    }
    HomePage.prototype.onPlantDetailsPage = function (plant) {
        console.log('plant: ', plant);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__plant_plant_details_plant_details__["a" /* PlantDetailsPage */], { plant: plant });
    };
    HomePage.prototype.onRemovePlant = function (plant) {
        this.plantService.removePlant(plant);
        this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
    };
    HomePage.prototype.getPlants = function () {
        return this.plantService.getPlants();
    };
    HomePage.prototype.onAddPlant = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_4__plant_add_plant_add_plant__["a" /* AddPlantPage */]);
        // this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
    };
    HomePage.prototype.ionViewWillEnter = function () {
        // this.plantService.setPlants(this.storageService.getData('plants-table'));
    };
    HomePage.prototype.initialize = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.storageService.getData('plants-table').then(function (data) {
            console.log('data: ', data);
            _this.plantService.setPlants(data);
            loader.dismiss();
        }).catch(function (err) {
            console.log(err);
            loader.dismiss();
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Plants</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n\n  <!-- <ion-item *ngFor="let plant of getPlants(); let i = index" (click)="onPlantDetailsPage(plant)">\n  \n    <h1>{{plant.name}}</h1>\n    <h3 style="overflow: hidden; width: 95%;">{{plant.description}}</h3>\n\n  </ion-item> -->\n\n  <ion-list>\n    <ion-item-sliding *ngFor="let plant of getPlants(); let i = index" (click)="onPlantDetailsPage(plant)">\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="{{ plant.image }}">\n        </ion-avatar>\n        <h2>{{plant.name}}</h2>\n        <p style="overflow: hidden; width: 95%;">{{plant.description}}</p>\n      </ion-item>\n      <ion-item-options side="right">\n        <button ion-button color="danger">\n          <ion-icon name="trash" color = "white" (click)="onRemovePlant(plant)"></ion-icon>\n          Remove\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab right bottom>\n    <button ion-fab (click)="onAddPlant()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__services_plant__["a" /* PlantService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__services_local_storage__["a" /* LocalStorageService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlantDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_plant__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_plants_list_modal_plants_list_modal__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_local_storage__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PlantDetailsPage = /** @class */ (function () {
    function PlantDetailsPage(navCtrl, navParams, plantService, modalCtrl, alertCtrl, storageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plantService = plantService;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.storageService = storageService;
        this.defaultImage = "../../../assets/imgs/default.png";
        this.plant = {
            name: "", description: null, image: this.defaultImage, light: null, water: null, size: null, temperature: null,
            symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        };
        this.plant = this.navParams.get("plant");
    }
    PlantDetailsPage.prototype.onRemovePlant = function () {
        this.showConfirm();
    };
    PlantDetailsPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Please confirm the action',
            message: 'Are you sure want to remove the plant?',
            buttons: [
                {
                    text: 'no',
                    handler: function () {
                    }
                },
                {
                    text: 'yes',
                    handler: function () {
                        _this.plantService.removePlant(_this.plant);
                        _this.storageService.saveDataRoot('plants-table', _this.plantService.getPlants());
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        confirm.present();
    };
    PlantDetailsPage.prototype.onStar = function (number, property) {
        this.plant[property] = number;
    };
    PlantDetailsPage.prototype.AddPlantPicture = function () {
    };
    PlantDetailsPage.prototype.onViewSymbios = function (type) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_plants_list_modal_plants_list_modal__["a" /* PlantsListModalComponent */], { plants: this.plant.symbiosis[__WEBPACK_IMPORTED_MODULE_2__services_plant__["b" /* SymbioticType */][type].toLowerCase()], type: type, list: [], symbiosis: false });
        modal.present();
    };
    PlantDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-plant-details',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/plant/plant-details/plant-details.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Plant Details</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-fab right top class="ches">\n      <button ion-fab (click)="onRemovePlant()">\n        <ion-icon color="white" name="trash"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <div class="main-pic">\n      <img src="{{ plant.image }}">\n    </div>\n\n    <hr />\n\n    <ion-item class="border-bottom">\n      <h1>{{ plant.name }}</h1>\n      <p>{{ plant.description }}</p>\n    </ion-item>\n\n    <ion-card>\n      <ion-card-header>\n        <h3><strong>Symbiotic relationships:</strong></h3>\n      </ion-card-header>\n\n      <ion-list>\n        <button ion-item style="margin-left: 10px;">\n          <!-- <ion-icon name="happy" item-end></ion-icon>\n             <ion-icon name="happy" item-end></ion-icon> -->\n          <button ion-button clear (click)="onViewSymbios(0)" item-end>View</button>\n          Mutualistic({{plant.symbiosis["mutualistic"].length}})\n        </button>\n\n        <button ion-item style="margin-left: 10px;">\n          <!-- <ion-icon name="happy" item-end></ion-icon>\n             <ion-icon name="sad" item-end></ion-icon> -->\n          <button ion-button clear (click)="onViewSymbios(1)" item-end>View</button>\n          Parasitic({{plant.symbiosis["parasitic"].length}})\n        </button>\n\n        <button ion-item style="margin-left: 10px;">\n          <!-- <ion-icon name="sad" item-end></ion-icon>\n             <ion-icon name="sad" item-end></ion-icon> -->\n          <button ion-button clear (click)="onViewSymbios(2)" item-end>View</button>\n          Competition({{plant.symbiosis["competition"].length}})\n        </button>\n\n        <button ion-item style="margin-left: 10px;">\n          <!-- <ion-icon name="sad" item-end></ion-icon>\n             <ion-icon name="sad" item-end></ion-icon> -->\n          <button ion-button clear (click)="onViewSymbios(3)" item-end>View</button>\n          Commensalistic({{plant.symbiosis["commensalistic"].length}})\n        </button>\n\n      </ion-list>\n    </ion-card>\n\n\n\n\n    <ion-item class="plantProperties">\n      <ion-row class="flex-center">\n        <ion-col col-1>\n          <ion-icon name="water" item-start class="icon-water"></ion-icon>\n        </ion-col>\n        <ion-col col-8>\n          Water Consumption\n        </ion-col>\n        <ion-col col-3>\n          <ion-icon name="star" *ngIf="plant.water >= 1"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.water >= 2"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.water >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" *ngIf="plant.water < 1"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.water < 2"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.water < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <ion-item class="plantProperties">\n      <ion-row class="flex-center">\n        <ion-col col-1>\n          <ion-icon name="sunny" item-start class="icon-sunny"></ion-icon>\n        </ion-col>\n        <ion-col col-8>\n          Light Consumption\n        </ion-col>\n        <ion-col col-3>\n          <ion-icon name="star" *ngIf="plant.light >= 1"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.light >= 2"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.light >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" *ngIf="plant.light < 1"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.light < 2"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.light < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <ion-item class="plantProperties">\n      <ion-row class="flex-center">\n        <ion-col col-1>\n          <ion-icon name="thermometer" item-start class="icon-thermometer"></ion-icon>\n        </ion-col>\n        <ion-col col-8>\n          Temperature\n        </ion-col>\n        <ion-col col-3>\n          <ion-icon name="star" *ngIf="plant.temperature >= 1"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.temperature >= 2"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.temperature >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" *ngIf="plant.temperature < 1"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.temperature < 2"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.temperature < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <ion-item class="plantProperties">\n      <ion-row class="flex-center">\n        <ion-col col-1>\n          <ion-icon name="resize" item-start class="icon-resize"></ion-icon>\n        </ion-col>\n        <ion-col col-8>\n          Size\n        </ion-col>\n        <ion-col col-3>\n          <ion-icon name="star" *ngIf="plant.size >= 1"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.size >= 2"></ion-icon>\n          <ion-icon name="star" *ngIf="plant.size >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" *ngIf="plant.size < 1"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.size < 2"></ion-icon>\n          <ion-icon name="star-outline" *ngIf="plant.size < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/plant/plant-details/plant-details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_plant__["a" /* PlantService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__services_local_storage__["a" /* LocalStorageService */]])
    ], PlantDetailsPage);
    return PlantDetailsPage;
}());

//# sourceMappingURL=plant-details.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPlantPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_plant__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_plants_list_modal_plants_list_modal__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_local_storage__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddPlantPage = /** @class */ (function () {
    function AddPlantPage(app, navParams, navCtrl, plantService, toastCtrl, modalCtrl, loadingCtrl, camera, storageService) {
        this.app = app;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.plantService = plantService;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.storageService = storageService;
        this.defaultImage = "../../../assets/imgs/default.png";
        this.plant = {
            name: "", description: null, image: this.defaultImage, light: 0, water: 0, size: 0, temperature: 0, symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        };
    }
    AddPlantPage.prototype.onGallery = function () {
        this.openCamera();
    };
    AddPlantPage.prototype.openCamera = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'loading...'
        });
        loader.present();
        var cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        };
        this.camera.getPicture(cameraOptions).then(function (imageData) {
            var img = 'data:image/jpeg;base64,' + imageData;
            _this.plant.image = img;
            loader.dismiss();
        }).catch(function (err) { return loader.dismiss(); });
    };
    AddPlantPage.prototype.onAddPlant = function () {
        console.log('this.plant: ', this.plant);
        var result = this.plantService.addPlant(this.plant);
        if (!result) {
            this.presentToast();
        }
        else {
            this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
        }
        this.app.getRootNav().pop();
    };
    AddPlantPage.prototype.onStar = function (number, property) {
        this.plant[property] = number;
    };
    AddPlantPage.prototype.onAddSymbios = function (type) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_plants_list_modal_plants_list_modal__["a" /* PlantsListModalComponent */], { plants: this.plantService.getPlants(), type: type, list: this.plant.symbiosis[__WEBPACK_IMPORTED_MODULE_2__services_plant__["b" /* SymbioticType */][type].toLowerCase()], symbiosis: true });
        modal.onDidDismiss(function (data) {
            _this.plant.symbiosis[__WEBPACK_IMPORTED_MODULE_2__services_plant__["b" /* SymbioticType */][type].toLowerCase()] = data;
        });
        modal.present();
    };
    AddPlantPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            position: "top",
            message: 'You already have this plant',
            duration: 6000
        });
        toast.present();
    };
    AddPlantPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-plant',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/plant/add-plant/add-plant.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>New Plant</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card class="padding-except-top">\n\n    <div class="main-pic" (click)="onGallery()">\n      <img src="{{ plant.image }}">\n    </div>\n\n    <hr/>\n\n    <ion-item>\n      <ion-label fixed>Name:</ion-label>\n      <ion-input [(ngModel)]="plant.name"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed>Description:</ion-label>\n      <ion-input [(ngModel)]="plant.description" type="text"></ion-input>\n    </ion-item>\n\n    <hr/>\n\n    <ion-title class="padding-small">\n      <h2>Add symbiotic relationships</h2>\n    </ion-title>\n\n    <hr/>\n\n    <ion-list>\n      <button ion-item (click)="onAddSymbios(0)">\n        <ion-icon name="happy" item-end></ion-icon>\n        <ion-icon name="happy" item-end></ion-icon>\n        <span class="colored">Mutualistic</span>\n      </button>\n\n      <button ion-item (click)="onAddSymbios(1)">\n        <ion-icon name="happy" item-end></ion-icon>\n        <ion-icon name="sad" item-end></ion-icon>\n        <span class="colored">Parasitic</span>\n      </button>\n\n      <button ion-item (click)="onAddSymbios(2)">\n        <ion-icon name="sad" item-end></ion-icon>\n        <ion-icon name="sad" item-end></ion-icon>\n        <span class="colored">Competition</span>\n      </button>\n\n      <button ion-item (click)="onAddSymbios(3)">\n        <ion-icon name="happy" item-end></ion-icon>\n        <ion-icon name="sad" item-end></ion-icon>\n        <span class="colored">Commensalistic</span>\n      </button>\n\n    </ion-list>\n    <hr/>\n\n    <ion-item>\n      <ion-row>\n        <ion-col col-8 no-padding class="flex-center">\n          Water Consumption:\n        </ion-col>\n        <ion-col col-4>\n          <ion-icon name="star" (click)="onStar(0, \'water\')" *ngIf="plant.water >= 1"></ion-icon>\n          <ion-icon name="star" (click)="onStar(1, \'water\')" *ngIf="plant.water >= 2"></ion-icon>\n          <ion-icon name="star" (click)="onStar(2, \'water\')" *ngIf="plant.water >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" (click)="onStar(1, \'water\')" *ngIf="plant.water < 1"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(2, \'water\')" *ngIf="plant.water < 2"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(3, \'water\')" *ngIf="plant.water < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <hr />\n    <ion-item>\n      <ion-row>\n        <ion-col col-8 no-padding class="flex-center">\n          Light Consumption:\n        </ion-col>\n        <ion-col col-4>\n          <ion-icon name="star" (click)="onStar(0, \'light\')" *ngIf="plant.light >= 1"></ion-icon>\n          <ion-icon name="star" (click)="onStar(1, \'light\')" *ngIf="plant.light >= 2"></ion-icon>\n          <ion-icon name="star" (click)="onStar(2, \'light\')" *ngIf="plant.light >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" (click)="onStar(1, \'light\')" *ngIf="plant.light < 1"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(2, \'light\')" *ngIf="plant.light < 2"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(3, \'light\')" *ngIf="plant.light < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <hr />\n    <ion-item >\n      <ion-row>\n        <ion-col col-8 no-padding class="flex-center">\n          Temperature:\n        </ion-col>\n        <ion-col col-4>\n          <ion-icon name="star" (click)="onStar(0, \'temperature\')" *ngIf="plant.temperature >= 1"></ion-icon>\n          <ion-icon name="star" (click)="onStar(1, \'temperature\')" *ngIf="plant.temperature >= 2"></ion-icon>\n          <ion-icon name="star" (click)="onStar(2, \'temperature\')" *ngIf="plant.temperature >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" (click)="onStar(1, \'temperature\')" *ngIf="plant.temperature < 1"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(2, \'temperature\')" *ngIf="plant.temperature < 2"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(3, \'temperature\')" *ngIf="plant.temperature < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <hr />\n    <ion-item >\n      <ion-row>\n        <ion-col col-8 no-padding class="flex-center">\n          Size:\n        </ion-col>\n        <ion-col col-4>\n          <ion-icon name="star" (click)="onStar(0, \'size\')" *ngIf="plant.size >= 1"></ion-icon>\n          <ion-icon name="star" (click)="onStar(1, \'size\')" *ngIf="plant.size >= 2"></ion-icon>\n          <ion-icon name="star" (click)="onStar(2, \'size\')" *ngIf="plant.size >= 3"></ion-icon>\n\n          <ion-icon name="star-outline" (click)="onStar(1, \'size\')" *ngIf="plant.size < 1"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(2, \'size\')" *ngIf="plant.size < 2"></ion-icon>\n          <ion-icon name="star-outline" (click)="onStar(3, \'size\')" *ngIf="plant.size < 3"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <hr />\n\n    <div class="full-width flex-center">\n      <button ion-button [disabled]="!plant.name || !plant.description" (click)="onAddPlant()">\n        Add the plant\n      </button>\n    </div>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/plant/add-plant/add-plant.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_plant__["a" /* PlantService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__services_local_storage__["a" /* LocalStorageService */]])
    ], AddPlantPage);
    return AddPlantPage;
}());

//# sourceMappingURL=add-plant.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(227);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_suggestion_suggestion__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_plant_plant_details_plant_details__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_plant_add_plant_add_plant__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_plant__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_plants_list_plants_list__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_plants_list_modal_plants_list_modal__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_local_storage__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_storage__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_suggestion_suggestion__["a" /* SuggestionPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_plant_plant_details_plant_details__["a" /* PlantDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_plant_add_plant_add_plant__["a" /* AddPlantPage */],
                __WEBPACK_IMPORTED_MODULE_13__components_plants_list_plants_list__["a" /* PlantsListComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_plants_list_modal_plants_list_modal__["a" /* PlantsListModalComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_17__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_suggestion_suggestion__["a" /* SuggestionPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_plant_plant_details_plant_details__["a" /* PlantDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_plant_add_plant_add_plant__["a" /* AddPlantPage */],
                __WEBPACK_IMPORTED_MODULE_13__components_plants_list_plants_list__["a" /* PlantsListComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_plants_list_modal_plants_list_modal__["a" /* PlantsListModalComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__["a" /* Camera */],
                Storage,
                __WEBPACK_IMPORTED_MODULE_12__services_plant__["a" /* PlantService */],
                __WEBPACK_IMPORTED_MODULE_16__services_local_storage__["a" /* LocalStorageService */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlantsListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlantsListComponent = /** @class */ (function () {
    function PlantsListComponent(app) {
        this.app = app;
        this.plants = [];
        this.type = "";
        this.symbiosis = false;
        this.list = [];
        this.onPlantPressed = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onAddPlantPressed = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onRemovePlantPressed = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.plantsView = [];
    }
    PlantsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.plantsView = JSON.parse(JSON.stringify(this.plants));
        //TODO: REFACTOR
        this.plantsView.forEach(function (p) {
            p.exist = false;
        });
        this.plantsView.forEach(function (p) {
            _this.list.forEach(function (e) {
                if (p.name == e.name)
                    p.exist = true;
            });
        });
    };
    PlantsListComponent.prototype.onAddPlant = function (i) {
        this.plantsView[i].exist = true;
        this.onAddPlantPressed.emit(this.plants[i]);
    };
    PlantsListComponent.prototype.onRemovePlant = function (i) {
        this.plantsView[i].exist = false;
        this.onRemovePlantPressed.emit(this.plants[i]);
    };
    PlantsListComponent.prototype.exist = function (plant) {
        return (this.plants.find(function (p) { return p.name == plant.name; })) ? true : false;
    };
    PlantsListComponent.prototype.onPlantClick = function (plant) {
        this.onPlantPressed.emit(plant);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("plants"),
        __metadata("design:type", Array)
    ], PlantsListComponent.prototype, "plants", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("type"),
        __metadata("design:type", String)
    ], PlantsListComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("symbiosis"),
        __metadata("design:type", Boolean)
    ], PlantsListComponent.prototype, "symbiosis", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("existedList"),
        __metadata("design:type", Array)
    ], PlantsListComponent.prototype, "list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], PlantsListComponent.prototype, "onPlantPressed", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], PlantsListComponent.prototype, "onAddPlantPressed", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], PlantsListComponent.prototype, "onRemovePlantPressed", void 0);
    PlantsListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'plants-list',template:/*ion-inline-start:"/Users/spasibov/Dev/planter/src/components/plants-list/plants-list.html"*/'    <ion-list *ngIf="symbiosis">\n      <button ion-item *ngFor="let plant of plantsView; let i = index">\n  \n        <h1>{{plant.name}}</h1>\n        <p>{{plant.description}}</p>\n  \n        <ion-icon name="add" item-end *ngIf="symbiosis && !plant.exist"  (click)="onAddPlant(i)"></ion-icon>\n        <ion-icon name="remove" item-end *ngIf="symbiosis && plant.exist" (click)="onRemovePlant(i)"></ion-icon>\n  \n      </button>\n    </ion-list>\n\n    <ion-list *ngIf="!symbiosis">\n      <ion-item  *ngFor="let plant of plantsView; let i = index" (click)="onPlantClick(plant)">\n  \n        <h1>{{plant.name}}</h1>\n        <h3 style="padding-right: 5px;">{{plant.description}}</h3>\n      </ion-item>\n    </ion-list>\n'/*ion-inline-end:"/Users/spasibov/Dev/planter/src/components/plants-list/plants-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], PlantsListComponent);
    return PlantsListComponent;
}());

//# sourceMappingURL=plants-list.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlantService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SymbioticType; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlantService = /** @class */ (function () {
    function PlantService() {
        this.plants = [];
        // this.initialize();
    }
    PlantService.prototype.addPlant = function (plant) {
        if (!this.plants.find(function (n) { return plant.name == n.name; })) {
            this.plants.push(plant);
            console.log('this.plants: ', this.plants);
            return true;
        }
        return false;
    };
    PlantService.prototype.removePlant = function (plant) {
        this.plants = this.plants.filter(function (p) { return p.name != plant.name; });
    };
    PlantService.prototype.getPlants = function () {
        return this.plants;
    };
    PlantService.prototype.setPlants = function (plants) {
        this.plants = plants;
        console.log(this.plants);
    };
    PlantService.prototype.initialize = function () {
        this.plants.push({
            name: "Avens",
            description: "Avens is a woody perennial flowering plant of the genus Avens, in the family Rosaceae, or the flower it bears.",
            image: "../../../assets/imgs/default.png",
            temperature: 1,
            light: 3,
            water: 2,
            size: 1,
            symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        }, {
            name: "Butterbur",
            description: "A Butterbur is a woody perennial flowering plant of the genus Butterbur, in the family Rosaceae, or the flower it bears.",
            image: "../../../assets/imgs/default.png",
            temperature: 2,
            light: 3,
            water: 3,
            size: 3,
            symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        }, {
            name: "Bunny Ear Cactus",
            description: "A Bunny Ear Cactus is a woody perennial flowering plant of the genus Bunny Ear Cactus, in the family Rosaceae, or the flower it bears.",
            image: "../../../assets/imgs/default.png",
            temperature: 2,
            light: 3,
            water: 3,
            size: 3,
            symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        }, {
            name: "Genista",
            description: "A Genista is a woody perennial flowering plant of the genus Genista, in the family Rosaceae, or the flower it bears.",
            image: "../../../assets/imgs/default.png",
            temperature: 2,
            light: 3,
            water: 3,
            size: 3,
            symbiosis: {
                mutualistic: [],
                parasitic: [],
                competition: [],
                commensalistic: []
            }
        });
    };
    PlantService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], PlantService);
    return PlantService;
}());

// Bro this bullshit start with 0 f.e. if you say let a = SymbioticType.Parasitic; a will be equal to 1
var SymbioticType;
(function (SymbioticType) {
    SymbioticType[SymbioticType["Mutualistic"] = 0] = "Mutualistic";
    SymbioticType[SymbioticType["Parasitic"] = 1] = "Parasitic";
    SymbioticType[SymbioticType["Competition"] = 2] = "Competition";
    SymbioticType[SymbioticType["Commensalistic"] = 3] = "Commensalistic";
})(SymbioticType || (SymbioticType = {}));
//# sourceMappingURL=plant.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalStorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LocalStorageService = /** @class */ (function () {
    function LocalStorageService(storage) {
        this.storage = storage;
    }
    LocalStorageService.prototype.getData = function (rootName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.exist(rootName).then(function (exist) {
                if (exist) {
                    _this.getDataRoot(rootName).then(function (data) {
                        resolve(data);
                    });
                }
                else {
                    reject("The " + rootName + " table doesn't exist!");
                }
            });
        });
    };
    LocalStorageService.prototype.saveDataRoot = function (rootName, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.set(rootName, JSON.stringify(data)).then(function () { return resolve(); });
        });
    };
    LocalStorageService.prototype.exist = function (rootName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.keys().then(function (data) {
                var result = data.includes(rootName) ? true : false;
                resolve(result);
            }).catch(function (err) {
                console.log('error in local storage: ' + err);
                reject(false);
            });
        });
    };
    LocalStorageService.prototype.getDataRoot = function (rootName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get(rootName).then(function (data) { return resolve(JSON.parse(data)); });
        });
    };
    LocalStorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], LocalStorageService);
    return LocalStorageService;
}());

//# sourceMappingURL=local-storage.js.map

/***/ })

},[204]);
//# sourceMappingURL=main.js.map