import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { App } from 'ionic-angular';
import { PlantModel } from '../../services/plant';
import { ThrowStmt } from '@angular/compiler';

@Component({
    selector: 'plants-list',
    templateUrl: 'plants-list.html'
})
export class PlantsListComponent implements OnInit {

    @Input("plants") plants: PlantModel[] = [];

    @Input("type") type: string = "";

    @Input("plant") plant: PlantModel = null;

    @Input("symbiosis") symbiosis: boolean = false;

    @Input("existedList") list: any[] = [];

    @Output() onPlantPressed = new EventEmitter<PlantModel>();
    @Output() onAddPlantPressed = new EventEmitter<PlantModel>();
    @Output() onRemovePlantPressed = new EventEmitter<PlantModel>();


    plantsView: any[] = [];

    constructor(public app: App) {

    }

    ngOnInit() {
        console.log('this.plants: ', this.plants);
        let stringify = require('json-stringify-safe');

        this.plantsView = JSON.parse(stringify(this.plants));

        if (this.plant)
            this.plantsView = this.plantsView.filter((p) => p.name != this.plant.name);

        //TODO: REFACTOR
        this.plantsView.forEach((p) => {
            p.exist = false;
        });

        this.plantsView.forEach((p) => {
            this.list.forEach((e) => {
                if (p.name == e.name)
                    p.exist = true;
            });
        });
    }

    onAddPlant(i: number) {
        this.plantsView[i].exist = true;
        this.onAddPlantPressed.emit(this.plants[i]);
    }

    onRemovePlant(i: number) {
        this.plantsView[i].exist = false;
        this.onRemovePlantPressed.emit(this.plants[i]);

    }

    exist(plant) {
        return (this.plants.find(p => p.name == plant.name)) ? true : false;
    }

    onPlantClick(plant: PlantModel) {
        this.onPlantPressed.emit(plant);
    }

}
