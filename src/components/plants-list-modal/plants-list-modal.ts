import { Component } from '@angular/core';
import { App, NavParams, ViewController, ToastController } from 'ionic-angular';
import { PlantModel, SymbioticType } from '../../services/plant';
import { platformBrowser } from '@angular/platform-browser';
import { TypeScriptEmitter } from '@angular/compiler';

@Component({
    selector: 'plants-list-modal',
    templateUrl: 'plants-list-modal.html'
})
export class PlantsListModalComponent {

    plants: PlantModel[] = [];

    operation: string = "";

    type: string = "";

    list: PlantModel[] = [];

    symbiosis: boolean = true;

    plant: PlantModel = null;

    constructor(public app: App, private navParams: NavParams, private viewCtrl: ViewController, private toastCtrl: ToastController) {
        this.plants = this.navParams.get("plants");
        this.symbiosis = this.navParams.get('symbiosis');
        this.list = this.navParams.get('list');
        this.type = SymbioticType[Number(this.navParams.get("type"))];
        this.plant = this.navParams.get('plant');
    }

    onAddPlant(plant: PlantModel) {
        (this.list.find(p => p.name == plant.name)) ? this.presentToast() : this.list.push(plant);
    }

    onRemovePlant(plant: PlantModel) {
        this.list = this.list.filter((p) => p.name != plant.name);
    }

    presentToast() {
        const toast = this.toastCtrl.create({
            position: "top",
            message: 'This plant is already in the list!',
            duration: 3000
        });
        toast.present();
    }

    onDismiss() {
        this.viewCtrl.dismiss(this.list);
    }
}
