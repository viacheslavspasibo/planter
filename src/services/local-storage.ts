import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class LocalStorageService {

    constructor(private storage: Storage) {
    }

    getData(rootName): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.exist(rootName).then(exist => {
                if (exist) {
                    this.getDataRoot(rootName).then((data) => {
                        resolve(data);
                    });
                }else{
                    reject("The " + rootName + " table doesn't exist!");
                }
            })
        });
    }

    saveDataRoot(rootName, data): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let stringify = require('json-stringify-safe');

            this.storage.set(rootName, stringify(data)).then(() => resolve());
        });
    }

    exist(rootName): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.storage.keys().then((data) => {
                let result = data.includes(rootName) ? true : false;
                resolve(result);
            }).catch(err => {
                console.log('error in local storage: ' + err);
                reject(false);
            });
        });
    }

    private getDataRoot(rootName): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.storage.get(rootName).then((data) => resolve(JSON.parse(data)));
        });
    }
}