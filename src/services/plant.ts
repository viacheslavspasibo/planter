import { Injectable } from "@angular/core";
@Injectable()
export class PlantService {

  plants: PlantModel[] = [];

  constructor() {
    // this.initialize();
  }

  addPlant(plant: PlantModel): boolean {

    if (!this.plants.find(n => plant.name == n.name)) {
      this.plants.push(plant);
      console.log('this.plants: ', this.plants);
      return true;
    }
    return false;

  }
  removePlant(plant: PlantModel) {
    this.plants = this.plants.filter((p) => p.name != plant.name);
  }

  getPlants() {
    return this.plants;
  }

  setPlants(plants: PlantModel[]) {
    this.plants = plants;
    console.log(this.plants);
  }

  initialize() {
    this.plants.push(
    {
      name: "Avens",
      description: "Avens is a woody perennial flowering plant of the genus Avens, in the family Rosaceae, or the flower it bears.",
      image: "../../../assets/imgs/default.png",
      temperature: 1,
      light: 3,
      water: 2,
      size: 1,
      symbiosis: {
        mutualistic: [],
        parasitic: [],
        competition: [],
        commensalistic: []
      }
    },
    {
      name: "Butterbur",
      description: "A Butterbur is a woody perennial flowering plant of the genus Butterbur, in the family Rosaceae, or the flower it bears.",
      image: "../../../assets/imgs/default.png",
      temperature: 2,
      light: 3,
      water: 3,
      size: 3,
      symbiosis: {
        mutualistic: [],
        parasitic: [],
        competition: [],
        commensalistic: []
      }
    },
    {
      name: "Bunny Ear Cactus",
      description: "A Bunny Ear Cactus is a woody perennial flowering plant of the genus Bunny Ear Cactus, in the family Rosaceae, or the flower it bears.",
      image: "../../../assets/imgs/default.png",
      temperature: 2,
      light: 3,
      water: 3,
      size: 3,
      symbiosis: {
        mutualistic: [],
        parasitic: [],
        competition: [],
        commensalistic: []
      }
    },
    {
      name: "Genista",
      description: "A Genista is a woody perennial flowering plant of the genus Genista, in the family Rosaceae, or the flower it bears.",
      image: "../../../assets/imgs/default.png",
      temperature: 2,
      light: 3,
      water: 3,
      size: 3,
      symbiosis: {
        mutualistic: [],
        parasitic: [],
        competition: [],
        commensalistic: []
      }
    }
    
    
    );
  }
}

export interface PlantModel {
  name: string;
  description: string,
  image: string,
  temperature: number,
  light: number,
  water: number,
  size: number,
  symbiosis: SymbiosisModel
}

export interface SymbiosisModel{
  mutualistic: any,
  parasitic: any,
  competition: any,
  commensalistic: any
}

// Bro this bullshit start with 0 f.e. if you say let a = SymbioticType.Parasitic; a will be equal to 1
export enum SymbioticType{
  Mutualistic,
  Parasitic,
  Competition,
  Commensalistic
}