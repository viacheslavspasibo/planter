import { Injectable } from "@angular/core";

@Injectable()
export class ShareService {

    data: any = null;

    constructor() {
    }

    pushData(data) {
        this.data = data;
    }

    getData() {
        return this.data;
    }

    hasData() {
        return (this.data) ? true : false;
    }

    clear(){
        this.data = null;
    }
}
