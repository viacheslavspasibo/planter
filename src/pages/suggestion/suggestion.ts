import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, App, Events } from 'ionic-angular';
import { LocalStorageService } from '../../services/local-storage';
import { PlantService, PlantModel, SymbioticType } from '../../services/plant';
import { utf8Encode } from '@angular/compiler/src/util';
import { PlantsListModalComponent } from '../../components/plants-list-modal/plants-list-modal';
import { PlantDetailsPage } from '../plant/plant-details/plant-details';
import { ShareService } from '../../services/share';

@Component({
  selector: 'page-suggestion',
  templateUrl: 'suggestion.html'
})
export class SuggestionPage {

  plants: PlantModel[] = [];

  activePlant: PlantModel;

  name: string = "";

  check = {
    noSymbiosis: false,
    hasMutualistic: false,
    hasParasitic: false,
    hasCompetition: false,
    hasCommensalistic: false
  };

  constructor(public navCtrl: NavController, private share: ShareService, private loadingCtrl: LoadingController, private storageService: LocalStorageService, private plantService: PlantService, private modalCtrl: ModalController, private app: App) {

  }

  getPlants(type: number) {
    return this.activePlant.symbiosis[SymbioticType[type].toLowerCase()].slice(0, 5);
  }

  search() {
    this.clear();

    this.plants = this.plantService.getPlants();

    this.plants.forEach((p) => {
      if (p.name.toLowerCase() === this.name.toLowerCase()) {
        this.activePlant = p;

        let hasSymbiosis = false;

        if (p.symbiosis.mutualistic.length > 0) {
          this.check.hasMutualistic = true;
          hasSymbiosis = true;
        }
        if (p.symbiosis.commensalistic.length > 0) {
          this.check.hasCommensalistic = true;
          hasSymbiosis = true;

        }
        if (p.symbiosis.parasitic.length > 0) {
          this.check.hasParasitic = true;
          hasSymbiosis = true;
        }
        if (p.symbiosis.competition.length > 0) {
          this.check.hasCompetition = true;
          hasSymbiosis = true;
        }
        if (!hasSymbiosis) {
          this.check.noSymbiosis = true;
        }
      }
    });
  }

  clear() {
    this.check.hasMutualistic = false;

    this.check.hasCommensalistic = false;

    this.check.hasParasitic = false;

    this.check.hasCompetition = false;

    this.check.noSymbiosis = false;

  }

  ionViewWillLeave() {
    console.log('leave')
  }

  ionViewWillEnter() {
    if (this.share.hasData()) {
      this.name = this.share.getData();
      this.share.clear();
      this.search();
    }
  }

  onViewSymbios(type: number) {
    let modal = this.modalCtrl.create(PlantsListModalComponent, { plant: this.activePlant, plants: this.activePlant.symbiosis[SymbioticType[type].toLowerCase()], type: type, list: [], symbiosis: false });
    modal.present();
  }

  onPlantDetailsPage(plant: PlantModel) {
    console.log('plant: ', plant);
    (this.app.getRootNav() as NavController).push(PlantDetailsPage, { plant: plant, allow: false });
  }

}
