import { Component } from '@angular/core';
import { NavController, NavParams, App, AlertController, ModalController, Events } from 'ionic-angular';
import { PlantModel, PlantService , SymbioticType} from '../../../services/plant';
import { PlantsListModalComponent } from '../../../components/plants-list-modal/plants-list-modal';
import { LocalStorageService } from '../../../services/local-storage';
import { ShareService } from '../../../services/share';
import { AddPlantPage } from '../add-plant/add-plant';


@Component({
    selector: 'page-plant-details',
    templateUrl: 'plant-details.html'
})
export class PlantDetailsPage {
    defaultImage = "../../../assets/imgs/default.png";

    allowDisplay = true;
    
    plant: PlantModel = {
        name: "", description: null, image: this.defaultImage, light: null, water: null, size: null, temperature: null, 
        symbiosis: {
            mutualistic: [],
            parasitic: [],
            competition: [],
            commensalistic: []
        }
    };

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private plantService: PlantService,
        private modalCtrl : ModalController,
        private alertCtrl: AlertController,
        private storageService: LocalStorageService,
        private share: ShareService,
        private app: App) {
        this.plant = this.navParams.get("plant");
        this.allowDisplay = (this.navParams.get("allow") === undefined)? true: false;
    }

    onGetSuggestion(){
        this.share.pushData(this.plant.name);
        this.app.navPop();
        this.app.getRootNav().getActiveChildNav().select(1);
        console.log('finish');
    }

    onRemovePlant() {
        this.showConfirm();
    }

    onEditPlant(){
        this.navCtrl.push(AddPlantPage, {plant: this.plant});
    }

    showConfirm() {
        const confirm = this.alertCtrl.create({
            title: 'Please confirm the action',
            message: 'Are you sure want to remove the plant?',
            buttons: [
                {
                    text: 'no',
                    handler: () => {
                    }
                },
                {
                    text: 'yes',
                    handler: () => {
                        this.plantService.removePlant(this.plant);
                        this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
                        this.navCtrl.pop();
                    }
                }
            ]
        });
        confirm.present();
    }
    onStar(number, property) {
        this.plant[property] = number;
    }


    onViewSymbios(type: number) {
        let modal = this.modalCtrl.create(PlantsListModalComponent, { plant: this.plant, plants: this.plant.symbiosis[SymbioticType[type].toLowerCase()], type: type, list: [], symbiosis: false });
        modal.present();
    }
}



