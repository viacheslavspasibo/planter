import { Component } from '@angular/core';
import { NavController, NavParams, App, ToastController, ModalController, LoadingController } from 'ionic-angular';
import { PlantModel, PlantService, SymbioticType } from '../../../services/plant';
import { PlantsListModalComponent } from '../../../components/plants-list-modal/plants-list-modal';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LocalStorageService } from '../../../services/local-storage';

@Component({
    selector: 'page-add-plant',
    templateUrl: 'add-plant.html'
})
export class AddPlantPage {

    defaultImage = "../../../assets/imgs/default.png";

    pageType = "add"; // add or edit

    plant: PlantModel = {
        name: "", description: null, image: this.defaultImage, light: 0, water: 0, size: 0, temperature: 0, symbiosis: {
            mutualistic: [],
            parasitic: [],
            competition: [],
            commensalistic: []
        }
    };

    originalPlant: PlantModel = null;

    constructor(
        private app: App,
        private navParams: NavParams,
        public navCtrl: NavController,
        private plantService: PlantService,
        private toastCtrl: ToastController,
        private modalCtrl: ModalController,
        private loadingCtrl: LoadingController,
        private camera: Camera,
        private storageService: LocalStorageService) {
            let plant = this.navParams.get('plant');

            if(plant !== undefined){
                this.plant = this.originalPlant = plant;
                this.pageType = 'edit';
            }
    }

    onGallery() {
        this.openCamera();
    }

    openCamera() {
        let loader = this.loadingCtrl.create({
            content: 'loading...'
        });
        loader.present();

        const cameraOptions: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        };

        this.camera.getPicture(cameraOptions).then((imageData) => {
            let img = 'data:image/jpeg;base64,' + imageData;
            this.plant.image = img;
            loader.dismiss();
        }).catch((err) => loader.dismiss());

    }

    onAddPlant() {
        console.log('this.plant: ', this.plant);
        let result = this.plantService.addPlant(this.plant);
        if (!result) {
            this.presentToast();
        } else {
            this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
            (this.app.getRootNav() as NavController).pop();
        }
    }

    onEditPlant(){
        this.plantService.removePlant(this.originalPlant);
        this.onAddPlant();
    }

    onStar(number, property) {
        this.plant[property] = number;
    }

    onAddSymbios(type: number) {
        let modal = this.modalCtrl.create(PlantsListModalComponent, { plant: this.plant, plants: this.plantService.getPlants(), type: type, list: this.plant.symbiosis[SymbioticType[type].toLowerCase()], symbiosis: true });

        modal.onDidDismiss((data) => {
            this.plant.symbiosis[SymbioticType[type].toLowerCase()] = data;
        });

        modal.present();
    }

    presentToast() {
        const toast = this.toastCtrl.create({
            position: "top",
            message: 'You already have this plant',
            duration: 6000
        });
        toast.present();
    }

}



