import { Component } from '@angular/core';

import { SuggestionPage } from '../suggestion/suggestion';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SuggestionPage;

  constructor() {

  }
}
