import { Component } from '@angular/core';
import { NavController, App, LoadingController } from 'ionic-angular';
import { PlantModel, PlantService } from '../../services/plant';
import { PlantDetailsPage } from '../plant/plant-details/plant-details';
import { AddPlantPage } from '../plant/add-plant/add-plant';
import { LocalStorageService } from '../../services/local-storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    private app: App,
    private plantService: PlantService,
    private loadingCtrl: LoadingController,
    private storageService: LocalStorageService) {
    this.initialize();
  }

  onPlantDetailsPage(plant: PlantModel) {
    console.log('plant: ', plant);
    (this.app.getRootNav() as NavController).push(PlantDetailsPage, { plant: plant });
  }

  onRemovePlant(plant: PlantModel) {
    this.plantService.removePlant(plant);
    this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
  }

  getPlants() {
    return this.plantService.getPlants();
  }

  onAddPlant() {
    (this.app.getRootNav() as NavController).push(AddPlantPage);
    // this.storageService.saveDataRoot('plants-table', this.plantService.getPlants());
  }

  onEditPlant(plant) {
    (this.app.getRootNav() as NavController).push(AddPlantPage, { plant: plant });
  }

  ionViewWillEnter() {
    // this.plantService.setPlants(this.storageService.getData('plants-table'));
  }

  initialize() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();

    this.storageService.getData('plants-table').then(data => {
      console.log('data: ', data);
      this.plantService.setPlants(data);
      loader.dismiss();
    }).catch(err => {
      console.log(err);
      loader.dismiss();
    })
  }

}



