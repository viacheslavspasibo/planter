import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { SuggestionPage } from '../pages/suggestion/suggestion';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PlantDetailsPage } from '../pages/plant/plant-details/plant-details';
import { AddPlantPage } from '../pages/plant/add-plant/add-plant';
import { PlantService } from '../services/plant';
import { PlantsListComponent } from '../components/plants-list/plants-list';
import { PlantsListModalComponent } from '../components/plants-list-modal/plants-list-modal';
import { Camera } from '@ionic-native/camera';
import { LocalStorageService } from '../services/local-storage';
import { IonicStorageModule } from "@ionic/storage";
import { ShareService } from '../services/share';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    SuggestionPage,
    HomePage,
    TabsPage,
    PlantDetailsPage,
    AddPlantPage,
    PlantsListComponent,
    PlantsListModalComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    SuggestionPage,
    HomePage,
    TabsPage,
    PlantDetailsPage,
    AddPlantPage,
    PlantsListComponent,
    PlantsListModalComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Storage,
    PlantService,
    LocalStorageService,
    ShareService
  ]
})
export class AppModule {}
